/**********************************************
* File: MST.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains an STL implementation of an Adjacency List
* graph Minimum Spanning Tree solver 
*
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ g++ -g -std=c++11 -Wpedantic MST.cpp -o MST
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ ./MST

Output may be found at MST.out. Can be generated using ./MST > MST.out 

**********************************************/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <stack>
#include <fstream>
#include <queue>
#include <iterator>
#include <limits> // to set an integer to infinity 

#define ROOT 0

// data structure to store graph edges
template<class T>
struct Edge {
	T src;
	T dest;
	int weight;
	
   template <typename U>      // all instantiations of this template are friends
   /********************************************
   * Function Name  : operator<<
   * Pre-conditions :  std::ostream&, const Edge<U>& 
   * Post-conditions: friend std::ostream&
   * Overloaded Friend Operator<< to print an edge  
   ********************************************/
   friend std::ostream& operator<<( std::ostream&, const Edge<U>& );

};

template <typename T>
std::ostream& operator<<( std::ostream& os, const Edge<T>& theEdge) {
   
   std::cout << "{" << theEdge.src << " " << theEdge.dest << " " << theEdge.weight << "} ";
   
   return os;
}


template<class T>
struct Vertex {
	T value;
	mutable std::vector< Edge <T> > edges;
	
	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded == operator to compare vertex value  
	********************************************/
	bool operator==(const Vertex<T>& rhs) const{
		
		return value == rhs.value;
		
	}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded < operator to compare vertex values
	********************************************/
	bool operator<(const Vertex<T>& rhs) const{
		
		return value < rhs.value; 
		
	}
};

// class to represent a graph object
template<class T>
class Graph
{
	public:
		// construct a vectors to represent an adjacency list
		std::vector< Vertex<T> > adjList;
		
		// Hash Table to correlate Verticies with location in vector
		std::unordered_map< T, int > hashVerts;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges
		* Post-conditions: none
		*  
		* Graph Constructor
		********************************************/
		Graph(std::vector< Edge<T> > &edges, bool directional)
		{

			// add edges to the directed graph
			for (auto &edge: edges)
			{

				// Insert Origin 
				addEdge(edge);
				
				// Since this is undirected, put in opposite
				// Uncomment if graph is directional
				// Create a temp edge to reverse origin and destination 
				if(!directional){
					Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
					addEdge(tempEdge);
				}
				
			}
		}
		
		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*  
		********************************************/
		void addEdge(const Edge<T>& edge){
			
			// Create a temporary vertex with the source
			Vertex<T> tempVert;
			tempVert.value = edge.src;
			
			// Element was not found
			if(hashVerts.count(tempVert.value) == 0){
				
				// HashWords.insert( {wordIn, 1} );
				hashVerts.insert( {tempVert.value, adjList.size()} );
				
				// Inset the edge into the temporary vertex 
				// Serves as the first edge 
				tempVert.edges.push_back(edge);
				
				// Insert the vertex into the set 
				adjList.push_back(tempVert);
				
			}
			// Element was found!
			else{
				
				// Use the hash to get the location in adjList, then push onto the edges vector 
				adjList.at(hashVerts[tempVert.value]).edges.push_back(edge);
				
			}
		}
		
		/********************************************
		* Function Name  : returnHashLocation
		* Pre-conditions : T value
		* Post-conditions: int
		* 
		* Returns the location in the graph of the requested value 
		********************************************/
		// In Class Code Goes Here 
		int returnHashLocation(T value){
			
			return hashVerts[value];
		}
 
};

/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*  
* Prints all the elements in the graph 
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){
	
	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin: {Destination, Weight}" << std::endl;

	for(int iter = 0; iter < graph.adjList.size(); iter++){
		
		std::cout << graph.adjList.at(iter).value << ": ";
		
		for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){
			
			std::cout << graph.adjList.at(iter).edges.at(jter);
			
		}
		
		std::cout << std::endl;
	}
	
	std::cout << std::endl;
	
}

template<class T>
struct PriVerts {

	T value;
	mutable int weight;
	
	PriVerts(T value, int weight) : value(value), weight(weight) {}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const PriVerts<T>& rhs
	* Post-conditions: bool
	* 
	* Overloaded < operator for the PriVerts 
	********************************************/
	bool operator<(const PriVerts<T>& rhs) const{
		
		return rhs.weight < weight; 
		
	}

};	

template<class T>
struct MSTEdge{
	
	mutable T parent;
	mutable int weight;
	
	/********************************************
	* Function Name  : MSTEdge
	* Pre-conditions : none
	* Post-conditions: none
	* 
	* Empty Constructor 
	********************************************/
	MSTEdge() {}
	
	/********************************************
	* Function Name  : weight
	* Pre-conditions : weight
	* Post-conditions: MSTEdge(T parent, int weight) : parent(parent),
	* 
	* MSTEdge Constructor
	********************************************/
	MSTEdge(T parent, int weight) : parent(parent), weight(weight) {}
	
	/********************************************
	* Function Name  : operator=
	* Pre-conditions : const MSTEdge<T> rhs
	* Post-conditions: MSTEdge<T>&
	* 
	* Overloaded assignment operator necessary for Priority Queue 
	********************************************/
	MSTEdge<T>& operator=(const MSTEdge<T> rhs){
		
		return *this;
		
	}
	
};

template<class T>
/********************************************
* Function Name  : notInPQ
* Pre-conditions : std::priority_queue< PriVerts<T> >& vertPrior, T& elem
* Post-conditions: bool
* 
* Checks to see if element elem is in vertPrior Priority Queue 
********************************************/
bool notInPQ(std::priority_queue< PriVerts<T> >& vertPrior, T& elem){
	
	// Create a temporary priority queue
	std::priority_queue< PriVerts<T> > tempPrior = vertPrior;
	
	// While the temporary Queue isnot empty
	while(!tempPrior.empty()){
		
		// If the element is there, return false
		if(tempPrior.top().value == elem)
			return false;
		
		// Remove the element from the temporary Queue
		tempPrior.pop();
	}
	
	// Went through the entire queue and it was not found
	return true;
}

template<class T>
/********************************************
* Function Name  : printPQ
* Pre-conditions : std::priority_queue< PriVerts<T> >& vertPrior
* Post-conditions: none
* 
* Prints the contents of the current priority queue of PriVerts<T> 
********************************************/
void printPQ(std::priority_queue< PriVerts<T> >& vertPrior){
	
	// Create a temporary Priority Queue
	std::priority_queue< PriVerts<T> > tempPrior = vertPrior;
	
	// Empty the temporary queue
	while(!tempPrior.empty()){

		// Print the current queue
		std::cout << tempPrior.top().value << " " << tempPrior.top().weight << std::endl;
		
		tempPrior.pop();
	}

	std::cout << std::endl;
	
}

template<class T>
/********************************************
* Function Name  : printMSTEdges
* Pre-conditions : std::unordered_map< T, MSTEdge<T> >& solMST
* Post-conditions: none
* 
* Prints the current Minimum Spanning Tree Edges to the user 
********************************************/
void printMSTEdges(std::unordered_map< T, MSTEdge<T> >& solMST){
	
	// Iterate through the Hash Table 
	for(auto iter = solMST.begin(); iter != solMST.end(); iter++){
		
		// Print the Vertex, its parent, and the edge weight
		std::cout << iter->first << " " << iter->second.parent << " " << iter->second.weight << std::endl;
		
	}
	
	std::cout << std::endl;
	
}

template<class T>
/********************************************
* Function Name  : initMST
* Pre-conditions : Graph<T>& graph, std::unordered_map< T, MSTEdge<T> >& solMST, std::priority_queue< PriVerts<T> >& vertPrior
* Post-conditions: none
* 
* Initialize the Minimum Spanning Tree Elements as shown in the slides 
********************************************/
void initMST(Graph<T>& graph, std::unordered_map< T, MSTEdge<T> >& solMST, std::priority_queue< PriVerts<T> >& vertPrior){

	// Initialize the priority queue 
	PriVerts<T> rootVert(graph.adjList.at(0).value, 0);
	
	// Put the index and the vertex and the 0 in the top 
	vertPrior.push(rootVert);
	
	// Insert the root into the solution 
	solMST.insert( {graph.adjList.at(0).value , MSTEdge<T>('-', 0) } );
	
	// Iterate through the rest of the vertices and set their values to infinity
	for(int iter = 1; iter < graph.adjList.size(); iter++){
		
		if(notInPQ(vertPrior, graph.adjList.at(iter).value)){
			
			// std::numeric_limits gets the architecture and sets it to infinity equivalent
			PriVerts<T> tempVert(graph.adjList.at(iter).value, std::numeric_limits<int>::max());
			
			// Emplace the vertex and the infinity value in the queue 
			vertPrior.emplace(tempVert);
			
		}
		
		// In the event of an undirected graph, check for all destinations as well 
		for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){
			
			if(notInPQ(vertPrior, graph.adjList.at(iter).edges.at(jter).dest)){
			
				// std::numeric_limits gets the architecture and sets it to infinity equivalent
				PriVerts<T> tempVert(graph.adjList.at(iter).edges.at(jter).dest, std::numeric_limits<int>::max());
				
				// Emplace the vertex and the infinity value in the queue 
				vertPrior.emplace(tempVert);
			
			}
			
		}

	}

}

template< class T >
/********************************************
* Function Name  : updatePQElem
* Pre-conditions : std::priority_queue< PriVerts<T> >& vertPrior, PriVerts<T>& updatePV
* Post-conditions: none
* 
* Goes through the Priority Queue, and updates the value if the updatePV weight is 
* less than the element in the priority queue
********************************************/
void updatePQElem(std::priority_queue< PriVerts<T> >& vertPrior, PriVerts<T>& updatePV){
	
	// Create a temporary Priority Queue
	std::priority_queue< PriVerts<T> > tempPrior = vertPrior;
	
	// Empty the reference Priority Queue
	while(!vertPrior.empty()){
		vertPrior.pop();
	}

	// Go through the temp Queue
	while(!tempPrior.empty()){
		
		// Save the top element
		PriVerts<T> tempPV = tempPrior.top();
		tempPrior.pop();
		
		// If the value we are updating is equal
		if(updatePV.value == tempPV.value){
		
			if(updatePV.weight < tempPV.weight){
				// If the weight is less, put in the new weight
				vertPrior.emplace(updatePV);
			}
			else{
				// If the weight is not less, put the old value
				vertPrior.emplace(tempPV);
			}
		}
		// If it's not equal, we just want to put the original element
		else{
			vertPrior.emplace(tempPV);
		}
	}
	
}

template<class T>
/********************************************
* Function Name  : updateSolHash
* Pre-conditions : std::unordered_map< T, MSTEdge<T> >& solMST, T& value, PriVerts<T>& newEdge
* Post-conditions: none
* 
* Updates the Solution 
********************************************/
void updateSolHash(std::unordered_map< T, MSTEdge<T> >& solMST, T& value, PriVerts<T>& newEdge){
		
	if(solMST.count(newEdge.value) == 0){
		
		solMST.insert( {newEdge.value, MSTEdge<T>( value, newEdge.weight ) } );
	}
	else{
		solMST[newEdge.value].parent = value;
		solMST[newEdge.value].weight = newEdge.weight;
	}
	
}

template<class T>
/********************************************
* Function Name  : runMST
* Pre-conditions : Graph<T>& graph, std::unordered_map< T, MSTEdge<T> >& solMST, std::priority_queue< PriVerts<T> >& vertPrior
* Post-conditions: none
* 
* Runs the Minimum Spanning Tree Algorithm 
********************************************/
void runMST(Graph<T>& graph, std::unordered_map< T, MSTEdge<T> >& solMST, std::priority_queue< PriVerts<T> >& vertPrior){
	
	while(!vertPrior.empty()){
	
		// Set get a temporary Vertext based on the queue 
		Vertex<T>* currVert = &graph.adjList.at(graph.returnHashLocation(vertPrior.top().value));
	
		// Remove the top element from the Queue 
		vertPrior.pop();
	
		// Set a temporary Queue equal to vertPrior. This is the frontier
		std::priority_queue< PriVerts<T> > tempPQ = vertPrior;
	
		// Check each element in the frontier 
		while(!tempPQ.empty()){
			
			PriVerts<T> compPriVerts = tempPQ.top();
			
			// Iterate through each edge 
			for(int iter = 0; iter < currVert->edges.size(); iter++){
				
				// Compare the destination of the edge with the current element 
				// If found, then the element is still in the Frontier 
				// Otherwise, there is an edge back to a visited node
				if(currVert->edges.at(iter).dest == compPriVerts.value){
					
					// Compare the edge weight with the current value
					if(currVert->edges.at(iter).weight < compPriVerts.weight){
						
						// Update the edges Priority Element 
						compPriVerts.weight = currVert->edges.at(iter).weight;
						
						// Update the Priority Queue 
						updatePQElem(vertPrior, compPriVerts);
						
						// Update the Solution Edge Hash 
						updateSolHash(solMST, currVert->value, compPriVerts);
					}
					
				}
				
			}
			
			// remove the element from the queue 
			tempPQ.pop();
		}
		
		// Print the current Priority Queue to the user
		std::cout << "Next Priority Queue:" << std::endl;
		printPQ(vertPrior);
	
		// Print the current Hash Table to the user
		std::cout << "Next Solution Hash Table:" << std::endl;
		printMSTEdges(solMST);		
		
	}
	
}

template< class T>
/********************************************
* Function Name  : printMSTGraph
* Pre-conditions : std::unordered_map< T, MSTEdge<T> >& solMST
* Post-conditions: none
* 
* Prints the MST Final Graph given the solution Hash Table 
********************************************/
void printMSTGraph(std::unordered_map< T, MSTEdge<T> >& solMST){
	
	T origin;
	
	for(auto iter = solMST.begin(); iter != solMST.end(); iter++){
		
		if(iter->second.parent == '-'){
			origin = iter->first;
		}
		
	}
	
	// Create Edges 
	std::vector< Edge<T> > MSTedges;
	
	// Put in the edges from the origin
	for(auto iter = solMST.begin(); iter != solMST.end(); iter++){
		if(iter->second.parent == origin){
			
			// Create temporary edge			
			Edge<T> tempEdge; 
			// Set the edge source equal to the parent
			tempEdge.src = iter->second.parent;
			// Set the destination of the edge equal to the reference vertex
			tempEdge.dest = iter->first;
			// Make the weights the same
			tempEdge.weight = iter->second.weight;
			// Insert the edges
			MSTedges.push_back(tempEdge);
			
		}
	}
	
	// Put in all non-origin elements
	for(auto iter = solMST.begin(); iter != solMST.end(); iter++){
		if(iter->second.parent != origin && iter->second.parent != '-'){

			Edge<T> tempEdge; 			
			tempEdge.src = iter->second.parent;
			tempEdge.dest = iter->first;
			tempEdge.weight = iter->second.weight;
			MSTedges.push_back(tempEdge);
			
		}
	}
	
	std::cout << "Final Minimum Spanning Tree Directed Graph: " << std::endl;
	
	// construct graph with directed edges 
	Graph<char> MSTGraph(MSTedges, true);
	// print the initial graph
	printGraph(MSTGraph);

}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* Main Driver Function 
********************************************/
int main(int argc, char** argv)
{
	// Allocate the edges as shown in the description
	std::vector< Edge<char> > edges =
	{
		{'e', 'd', 4}, {'e', 'c', 5}, {'e', 'b', 5}, 
		{'d', 'c', 4}, {'d', 'b', 6}, {'d', 'a', 2}, 
		{'a', 'c', 5}, {'a', 'b', 9}
	};

	// construct graph with directed edges 
	Graph<char> charGraph(edges, false);
	
	// print the initial graph
	printGraph(charGraph);

	// Construct the MST Solution Hash Table 
	std::unordered_map< char, MSTEdge<char> > solMST;

	// Initialize the Priority Queue
	std::priority_queue< PriVerts<char> > vertPrior;

	// Initialize Algorithm
	initMST(charGraph, solMST, vertPrior);
	
	std::cout << "Initialized Priority Queue:" << std::endl;
	printPQ(vertPrior);
	
	std::cout << "Initialize Solution Hash Table:" << std::endl;
	printMSTEdges(solMST);
	
	// Run the Algorithm 
	runMST(charGraph, solMST, vertPrior);
	
	// Print the MST Solution Graph 
	printMSTGraph(solMST);

	return 0;
}
<<<<<<< HEAD

=======
>>>>>>> upstream/master
