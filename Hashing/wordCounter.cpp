#include <iostream>
#include <string>
#include <map>
#include <fstream>
using namespace std;

typedef  map<string, int> wordMap;

void wordCounter(istream& is, wordMap& words);

int main(int argc, char** argv){

	ifstream is(argv[1]);

	wordMap myMap;

	wordCounter(is, myMap);

	for(wordMap::iterator iter = myMap.begin(); iter != myMap.end(); iter++){
		cout << iter->first <<" occurs "<<iter->second<<" times"<<endl;
	}
}

void wordCounter(istream& is, wordMap& words){
	string str;
	while(is >> str){
		words[str]++;
	}
}
