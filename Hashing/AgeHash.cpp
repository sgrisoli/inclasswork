/**********************************************
* File: AgeHash.cpp
<<<<<<< HEAD
* Author: Stephen Grisoli
* Email: sgrisoli@nd.edu
=======
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
>>>>>>> DrMorrison/master
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
// Libraries Here
<<<<<<< HEAD
#include <iostream>
#include <string>
#include <map>
#include <unordered_map>
#include <iterator>
#include <vector>
=======
>>>>>>> DrMorrison/master

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){

<<<<<<< HEAD
	//std::map<key, value> hashName
	std::map<std::string, int> ageHashOrdered = { {"Matthew", 38}, {"Alfred", 72}, {"Rosco", 36}, {"James", 38} };

	std::map<std::string, int>::iterator iterOr;

	std::cout<<"The ordered ages are: " << std::endl;
  	for(iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++){
		std::cout << iterOr->first << " " << iterOr->second << std::endl;
	}
	
	std::cout << "The Ordered Example: " << ageHashOrdered["Matthew"] << std::endl << std::endl;

	std::unordered_map<std::string, int> ageHashUnordered = { {"Matthew", 38}, {"Alfred", 72}, {"Rosco", 36}, {"James", 38} };
	std::unordered_map<std::string, int>::iterator iterUn;

	std::cout<<"The unordered ages are: " << std::endl;

	for(iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++){
		std::cout <<iterUn->first << " " << iterUn->second << std::endl;
	}
	
	std::cout <<"The Unordered Example: " << ageHashUnordered["Alfred"] << std::endl;
	
	
	/*
	std::map<std::string, int, int> myExample = { {"Stephen", 22, 420} };
	std::cout << myExample.begin()->first << std::endl;
	std::cout << myExample.begin()->second << std::endl;
	std::cout << myExample.begin()->third << std::endl;
	*/

	/*std::vector<int> vec (10, 20);
	std::map<std::string, std::vector<int> > myExample = { {"Stephen", vec} };
	std::cout <<"My example is: "<< (myExample.begin())->first <<" "<< myExample.begin()->second.begin()<< myExample.begin()->second[1] << std::endl;
	*/

=======
	
>>>>>>> DrMorrison/master
	return 0;
}
