/**********************************************
* File: RBTree.h
* Author: Matthew Morrison, Pedro Lopes
* Email: matt.morrison@nd.edu, pdesousa@nd.edu
* 
* Contains the methods for a templated Red-Black Tree
**********************************************/
#ifndef RBTREE_H
#define RBTREE_H 

#include "RBTreeNode.h"
#include <queue>		// Level Order Traversal only


template<class T> 
class RBTree { 

	private:

		// Red-Black Tree root node 
		RBTNode<T> *root; 
			
		/********************************************
		* Function Name	: leftRotate
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: none
		* 
		* left rotates the given node	
		********************************************/
		void leftRotate(RBTNode<T> *x) { 
			// new parent will be node's right child 
			RBTNode<T> *nParent = x->right; 
			
			// update root if current node is root 
			if (x == root) 
				root = nParent; 
			
			x->moveDown(nParent); 
			
			// connect x with new parent's left element 
			x->right = nParent->left; 
			// connect new parent's left element with node 
			// if it is not null 
			if (nParent->left != NULL) 
				nParent->left->parent = x; 
			
			// connect new parent with x 
			nParent->left = x; 
		} 
			
		/********************************************
		* Function Name	: rightRotate
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: none
		* 
		* Performs right rotation on the node 
		********************************************/
		void rightRotate(RBTNode<T> *x) { 
			// new parent will be node's left child 
			RBTNode<T> *nParent = x->left; 
			
			// update root if current node is root 
			if (x == root) 
				root = nParent; 
			
			x->moveDown(nParent); 
			
			// connect x with new parent's right element 
			x->left = nParent->right; 
			// connect new parent's right element with node 
			// if it is not null 
			if (nParent->right != NULL) 
				nParent->right->parent = x; 
			
			// connect new parent with x 
			nParent->right = x; 
		} 
			
		/********************************************
		* Function Name	: swapColors
		* Pre-conditions : RBTNode<T> *x1, RBTNode<T> *x2
		* Post-conditions: none
		* 
		* Swaps the colors of the two input nodes
		********************************************/
		void swapColors(RBTNode<T> *x1, RBTNode<T> *x2) { 
			COLOR temp; 
			temp = x1->color; 
			x1->color = x2->color; 
			x2->color = temp; 
		} 
			
		/********************************************
		* Function Name	: swapValues
		* Pre-conditions : RBTNode<T> *u, RBTNode<T> *v
		* Post-conditions: none
		*	
		********************************************/
		void swapValues(RBTNode<T> *u, RBTNode<T> *v) { 
			T temp; 
			temp = u->val; 
			u->val = v->val; 
			v->val = temp; 
		} 
			
		/********************************************
		* Function Name	: fixRedRed
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: none
		* 
		* fix red red at given node
		********************************************/
		void fixRedRed(RBTNode<T> *x) { 
			// if x is root color it black and return 
			if (x == root) { 
				x->color = BLACK; 
				return; 
			} 
			
			// initialize parent, grandparent, uncle 
			RBTNode<T> *parent = x->parent, *grandparent = parent->parent, 
				 *uncle = x->uncle(); 
			
			if (parent->color != BLACK) { 
				if (uncle != NULL && uncle->color == RED) { 
					// uncle red, perform recoloring and recurse 
					parent->color = BLACK; 
					uncle->color = BLACK; 
					grandparent->color = RED; 
					fixRedRed(grandparent); 
				} 
				else { 
					// Else perform LR, LL, RL, RR 
					if (parent->isOnLeft()) { 
						if (x->isOnLeft()) { 
							// for left right 
							swapColors(parent, grandparent); 
						} 
						else { 
							leftRotate(parent); 
							swapColors(x, grandparent); 
						} 
						// for left left and left right 
						rightRotate(grandparent); 
					} 
					else { 
						if (x->isOnLeft()) { 
							// for right left 
							rightRotate(parent); 
							swapColors(x, grandparent); 
						} else { 
							swapColors(parent, grandparent); 
						} 
				
						// for right right and right left 
						leftRotate(grandparent); 
					} 
				} 
			} 
		} 
			
		// find node that do not have a left child 
		// in the subtree of the given node 
		/********************************************
		* Function Name	: successor
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: RBTNode<T>*
		*	
		********************************************/
		RBTNode<T>* successor(RBTNode<T> *x) { 
			RBTNode<T> *temp = x; 
			
			while (temp->left != NULL) 
				temp = temp->left; 
			
			return temp; 
		} 
			
		// find node that replaces a deleted node in BST 
		/********************************************
		* Function Name	: BSTreplace
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: RBTNode<T>*
		*	
		********************************************/
		RBTNode<T>* BSTreplace(RBTNode<T> *x) { 
			// when node have 2 children 
			if (x->left != NULL and x->right != NULL) 
				return successor(x->right); 
			
			// when leaf 
			if (x->left == NULL and x->right == NULL) 
				return NULL; 
			
			// when single child 
			if (x->left != NULL) 
				return x->left; 
			else
				return x->right; 
		} 
			
		/********************************************
		* Function Name	: deleteNode
		* Pre-conditions : RBTNode<T> *v
		* Post-conditions: none
		* 
		* deletes the given node
		********************************************/
		void deleteNode(RBTNode<T> *v) { 
			RBTNode<T> *u = BSTreplace(v); 
			
			// True when u and v are both black 
			bool uvBlack = ((u == NULL or u->color == BLACK) and (v->color == BLACK)); 
			RBTNode<T> *parent = v->parent; 
			
			if (u == NULL) { 
			// u is NULL therefore v is leaf 
				if (v == root) { 
					// v is root, making root null 
					root = NULL; 
				} else { 
				if (uvBlack) { 
				// u and v both black 
				// v is leaf, fix double black at v 
					fixDoubleBlack(v); 
				} else { 
				// u or v is red 
					if (v->sibling() != NULL) 
					// sibling is not null, make it red" 
					v->sibling()->color = RED; 
				} 
			
				// delete v from the tree 
				if (v->isOnLeft()) { 
					parent->left = NULL; 
				} else { 
					parent->right = NULL; 
				} 
			} 
				delete v; 
				return; 
			} 
			
			if (v->left == NULL or v->right == NULL) { 
			// v has 1 child 
				if (v == root) { 
				// v is root, assign the value of u to v, and delete u 
				v->val = u->val; 
				v->left = v->right = NULL; 
				delete u; 
			} else { 
				// Detach v from tree and move u up 
				if (v->isOnLeft()) { 
					parent->left = u; 
				} else { 
					parent->right = u; 
				} 
				delete v; 
				u->parent = parent; 
				if (uvBlack) { 
				// u and v both black, fix double black at u 
					fixDoubleBlack(u); 
				} else { 
				// u or v red, color u black 
					u->color = BLACK; 
				} 
			} 
				return; 
			} 
			
			// v has 2 children, swap values with successor and recurse 
			swapValues(u, v); 
			deleteNode(u); 
		} 
			
		/********************************************
		* Function Name	: fixDoubleBlack
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: none
		*	
		********************************************/
		void fixDoubleBlack(RBTNode<T> *x) { 
			if (x == root) 
			// Reached root 
				return; 
			
			RBTNode<T> *sibling = x->sibling(), *parent = x->parent; 
			if (sibling == NULL) { 
			// No sibiling, double black pushed up 
				fixDoubleBlack(parent); 
			} else { 
				if (sibling->color == RED) { 
				// Sibling red 
				parent->color = RED; 
				sibling->color = BLACK; 
				if (sibling->isOnLeft()) { 
				// left case 
					rightRotate(parent); 
				} else { 
				// right case 
					leftRotate(parent); 
				} 
				fixDoubleBlack(x); 
			} else { 
				// Sibling black 
				if (sibling->hasRedChild()) { 
				// at least 1 red children 
					if (sibling->left != NULL and sibling->left->color == RED) { 
					if (sibling->isOnLeft()) { 
					// left left 
						sibling->left->color = sibling->color; 
						sibling->color = parent->color; 
						rightRotate(parent); 
					} else { 
					// right left 
						sibling->left->color = parent->color; 
						rightRotate(sibling); 
						leftRotate(parent); 
					} 
				} else { 
					if (sibling->isOnLeft()) { 
					// left right 
						sibling->right->color = parent->color; 
						leftRotate(sibling); 
						rightRotate(parent); 
					} else { 
					// right right 
						sibling->right->color = sibling->color; 
						sibling->color = parent->color; 
						leftRotate(parent); 
					} 
				} 
					parent->color = BLACK; 
				} else { 
				// 2 black children 
					sibling->color = RED; 
					if (parent->color == BLACK) 
					fixDoubleBlack(parent); 
					else
					parent->color = BLACK; 
				} 
			} 
			} 
		} 
			
		/********************************************
		* Function Name	: levelOrder
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: none
		* 
		* Prints level order for given node
		********************************************/
		void levelOrder(RBTNode<T> *x) { 
			if (x == NULL) 
			// return if node is null 
				return; 
			
			// queue for level order 
			std::queue<RBTNode<T> *> q; 
			RBTNode<T> *curr; 
			
			// push x 
			q.push(x); 
			
			while (!q.empty()) { 
			// while q is not empty 
			// dequeue 
				curr = q.front(); 
				q.pop(); 
			
			// print node value 
				std::cout << curr->val << " "; 
			
			// push children to queue 
				if (curr->left != NULL) 
					q.push(curr->left); 
				if (curr->right != NULL) 
					q.push(curr->right);
			} 

		} 
			
		/********************************************
		* Function Name	: inorder
		* Pre-conditions : RBTNode<T> *x
		* Post-conditions: none
		* 
		* Prints inorder recursively 
		********************************************/
		void inorder(RBTNode<T> *x) { 
			if (x == NULL) 
				return; 
			inorder(x->left); 
			std::cout << x->val << " "; 
			inorder(x->right); 
		} 
		
	public: 
		/********************************************
		* Function Name	: RBTree
		* Pre-conditions : none
		* Post-conditions: none
		*	
		* Constructor. Initializes root to NULL
		********************************************/
		RBTree() { root = NULL; } 
			
		/********************************************
		* Function Name	: getRoot
		* Pre-conditions : none
		* Post-conditions: RBTNode<T>*
		* 
			
		********************************************/
		RBTNode<T>* getRoot() { return root; } 
			
 
		/********************************************
		* Function Name	: search
		* Pre-conditions : T n
		* Post-conditions: RBTNode<T>*
		*
		* searches for given value 
		* if found returns the node (used for delete) 
		* else returns the last node while traversing (used in insert)		
		********************************************/
		RBTNode<T>* search(T n) { 
			RBTNode<T> *temp = root; 
			while (temp != NULL) { 
				if (n < temp->val) { 
				if (temp->left == NULL) 
					break; 
				else
					temp = temp->left; 
			} else if (n == temp->val) { 
				break; 
			} else { 
				if (temp->right == NULL) 
					break; 
				else
					temp = temp->right; 
			} 
			} 
			
			return temp; 
		} 
			
		/********************************************
		* Function Name	: insert
		* Pre-conditions : T n
		* Post-conditions: none
		* 
		* Inserts the given value to tree 
		********************************************/
		void insert(T n) { 
			RBTNode<T> *newNode = new RBTNode<T>(n); 
			if (root == NULL) { 
			// when root is null 
			// simply insert value at root 
				newNode->color = BLACK; 
				root = newNode; 
			} else { 
			RBTNode<T> *temp = search(n); 
			
				if (temp->val == n) { 
				// return if value already exists 
				return; 
			} 
			
			// if value is not found, search returns the node 
			// where the value is to be inserted 
			
			// connect new node to correct node 
				newNode->parent = temp; 
			
				if (n < temp->val) 
				temp->left = newNode; 
				else
					temp->right = newNode; 
			
			// fix red red voilaton if exists 
				fixRedRed(newNode); 
			} 
		} 
			
		/********************************************
		* Function Name	: deleteByVal
		* Pre-conditions : int n
		* Post-conditions: none
		* 
		* Deletes the node with given value		
		********************************************/
		void deleteByVal(T n) { 
			if (root == NULL) 
				// Tree is empty 
				return; 
			
			RBTNode<T> *v = search(n), *u; 
			
			if (v->val != n) { 
				std::cout << "No node found to delete with value:" << n << std::endl; 
				return; 
			} 
			
			deleteNode(v); 
		} 
			
		/********************************************
		* Function Name	: printInOrder
		* Pre-conditions : none
		* Post-conditions: none
		* 
		* Prints an in-order traversal of the tree
		********************************************/
		void printInOrder() { 
			std::cout << "Inorder: " << std::endl; 
			if (root == NULL) 
				std::cout << "Tree is empty" << std::endl; 
			else
				inorder(root); 
			std::cout << std::endl; 
		} 
			
		/********************************************
		* Function Name	: printLevelOrder
		* Pre-conditions : none
		* Post-conditions: none
		* 
		* Prints level order of the tree 
		********************************************/
		void printLevelOrder() { 
			std::cout << "Level order: " << std::endl; 
			if (root == NULL) 
				std::cout << "Tree is empty" << std::endl; 
			else
				levelOrder(root); 
			std::cout << std::endl; 
		}

		int leftMost(){
 		  RBTNode<T>* temp = root;
                  while(temp->left != NULL){
 		    temp = temp->left;
		  }
		  return temp->val;
		}
		 
};

#endif
