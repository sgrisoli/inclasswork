/**********************************************
* File: RBTreeTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains a test for the Int and String Red-Black
* templated Trees 
**********************************************/
#include "RBTree.h"
#include <string>

/********************************************
* Function Name  : insertAndPrint
* Pre-conditions : RBTree<T>* tree, T value
* Post-conditions: none
*  
*  Inserts into the Red-Black Tree and prints the
*  in-order and level order traversal to the user
********************************************/
template<class T>
void insertAndPrint(RBTree<T>* tree, T value){

	std::cout << "Inserting: " << value << std::endl;
	tree->insert(value);
	tree->printInOrder(); 
	tree->printLevelOrder(); 
	std::cout << std::endl;
	
}

/********************************************
* Function Name  : deleteAndPrint
* Pre-conditions : RBTree<T>* tree, T value
* Post-conditions: none
*
* Deletes from the Red-Black Tree and Prints 
* the traversal to the user  
********************************************/
template<class T>
void deleteAndPrint(RBTree<T>* tree, T value){

	std::cout << "Deleting " << value << std::endl;
	tree->deleteByVal(value);
	tree->printInOrder(); 
	tree->printLevelOrder(); 
	std::cout << std::endl;
	
}
/********************************************
* Function Name  : leftMost
* Pre-conditions : RBTree<T>
* Post-conditions: void
*
* Struct find the leftmost process and delete it
********************************************/
/*
template<class T>
void leftMost(RBTree<T>* tree){
  if(tree == NULL)
    return; 
  leftMost(tree->left);
  deleteAndPrint(&tree, tree->value);
}
*/    
/********************************************
* Function Name  : Process
* Pre-conditions : int, int
* Post-conditions: struct
*
* Struct "Process to store procNum and vruntime 
********************************************/
	
struct Process{
  int procNum;
  int vruntime;

/********************************************
* Function Name  : operator<
* Pre-conditions : Process&
* Post-conditions: bool
*
* Overload < and return logic
********************************************/

bool operator<(const Process& pro){
  if(vruntime < pro.vruntime)
    return true;
  else 
    return false;
}
/********************************************
* Function Name  : operator>
* Pre-conditions : Process&
* Post-conditions: bool
*
* Overload > and return logic
********************************************/

bool operator>(const Process& pro){
  if(vruntime > pro.vruntime)
    return true;
  else
    return false;
}
/********************************************
* Function Name  : operator==
* Pre-conditions : Process&
* Post-conditions: bool
*
* Overload == and return logic
********************************************/

bool operator==(const Process& pro){
  if(vruntime == pro.vruntime)
    return true;
  else
    return false;
}
};



/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver file for the program  
********************************************/

int main(int argc, char** argv) { 

  RBTree<int> tree; 
    Process a = {10,10};
    Process b = {10,20};
    Process c = {10,30};
    Process d = {10,40};
    Process e = {10,50};
    Process f = {10,5};
    Process g = {10,1};
    Process h = {10,100};
    Process i = {10,12};
    Process j = {10,31};

	insertAndPrint(&tree, a.vruntime);
	insertAndPrint(&tree, b.vruntime);
	insertAndPrint(&tree, c.vruntime);
	insertAndPrint(&tree, d.vruntime);
	insertAndPrint(&tree, e.vruntime);
	insertAndPrint(&tree, f.vruntime);
	insertAndPrint(&tree, g.vruntime);
	insertAndPrint(&tree, h.vruntime);
	insertAndPrint(&tree, i.vruntime);
	insertAndPrint(&tree, j.vruntime);

	int z = tree.leftMost();
        deleteAndPrint(&tree, z);	
//	leftMost(&tree);

  return 0; 
} 
